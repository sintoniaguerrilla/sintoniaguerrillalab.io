# Landing

Landing para Sintonía Guerrilla: https://sintoniaguerrilla.gitlab.io/

______

## Canales

[![spotify](https://copykirov.files.wordpress.com/2020/05/spotify.png?w=220)](https://open.spotify.com/show/4msPlFSVWdN9CGhKBQALDb?si=xAz1RUuXSXedfLNlGHWYm) [![ivoox](https://copykirov.files.wordpress.com/2020/05/ivoox_r.png?w=220)](https://www.ivoox.com/podcast-sintonia-guerrilla_sq_f1850911_1.html) [![Internet Archive](https://copykirov.files.wordpress.com/2020/05/internet_archive.png?w=220)](https://archive.org/details/@sintoniaguerrilla)

________

## Redes

[![instagram](https://copykirov.files.wordpress.com/2020/05/instagram_blanco.png?w=90)](https://www.instagram.com/sintoniaguerrilla/)[![facebook](https://copykirov.files.wordpress.com/2020/05/facebook_w_256.png?w=90)](https://www.facebook.com/sintoniaguerrilla) [![twitter](https://copykirov.files.wordpress.com/2020/05/twitter_w_256.png?w=90)](https://twitter.com/sinto_guerrilla)
